var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();
console.log(plugins);

var rootInputDir = './src/';
var input = {
  images: rootInputDir + 'assets/**/*',
  index: rootInputDir + 'index.html',
  style: rootInputDir + 'styles/app.scss',
  styles: rootInputDir + 'styles/**/*.scss'
};

var output = './dest';

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

gulp.task('server', function() {
  gulp.src('dest')
    .pipe(plugins.serverLivereload({
      livereload: true,
      defaultFile: 'index.html',
      directoryListing: false,
      open: false
    }));
});

gulp.task('sass', function() {
  return gulp.src(input.style)
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass(sassOptions).on('error', plugins.sass.logError))
    .pipe(plugins.sourcemaps.write())
    .pipe(plugins.groupCssMediaQueries())
    .pipe(plugins.autoprefixer(autoprefixerOptions))
    .pipe(plugins.cssnano())
    .pipe(gulp.dest(output))
    ;
});

gulp.task('image', function () {
  return gulp.src(input.images)
    .pipe(plugins.image())
    .pipe(gulp.dest(output + '/assets'));
});

gulp.task('index', function () {
  return gulp.src(input.index)
    .pipe(gulp.dest(output));
});

gulp.task('watch', function() {
  return gulp.watch([input.styles, input.index], ['sass', 'index'])
    .on('change', function(event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('default', ['index', 'image', 'sass']);
